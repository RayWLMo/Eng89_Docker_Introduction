# Docker
## Introduction to Docker
### What is Docker?
- Docker is an open platform containerisation tool for developing, shipping, and running applications.
- The applications built are separated from the infrastructure so the software delivery is faster.
### Comparing Docker with VMs
- Running a large amount of applications on a small number of services is better with Docker
- Apps spin up faster because they don't need to emulate hardware
- Resources on Docker are shared between containers as with VMs, each one is allocated their own resources
### Benefits of using Docker
- The amount of IT resources used/needed is reduced
- Snapshot sizes are smaller
- Software is lightweight
### Microservice Architecture
Microservice architecture (AKA: Microservices) is a style of architecture that structures an application aas a collection of services that are:
- Owned by a small team
- Organised around business capabilities
- Independently deployable
- Loosely coupled
- Highly maintainable and testable

Microservices allow for the fast, frequent and robust delivery of large, complex applications.

It also enables an organisation to evolve its technology stack
### Microservice architecture vs Monolithic architecture
- Microservices are easier to understand than monolith architecture
- Microservice architecture can be harder to develop than monolithic architecture
- Microservice architecture can be harder to deploy than monolithic architecture
- Individual components can be scalable on microservice architecture which isn't possible on monolithic architecture.
### Is Microservice architecture always useful?
- Due to the complexity of microservice architecture, it can initially be difficult to implement
- Because each component is independent in microservice architecture, testing a microservices-based solution could prove to be difficult
- Do to microservices complex nature of individual components, all the connections need to be carefully handled.

## Docker Commands
### `docker run`
Command
```shell
docker run
```
- `-d` - For detachable mode
- `-p` - To define ports
#### Examples of `docker run`
```shell
# Ghost Website
docker run -d -p 2368:2368 ghost

# Docker Documentation
docker run -d -p 4000:4000 docs/docker.github.io
```
As these are ran on the localhost, the html files can be edited using the vim/nano editor inside the instance

### Accessing the instance
- First, an alias needs to be made so docker can communicate
```shell
alias docker="winpty docker"
```
- To enter an instance, this command is used:
```shell
docker exec
```
- Add `-it` to be able to interact with the instance
- It also needs the `Container ID` specified
- Add `sh` after the `Container ID` to ssh into the container

The complete command should look like this
```shell
docker exec -it container_id_goes_here sh
```

All these commands are followed by the `Container ID` of the desired container

- `docker stop` to stop a running container
- `docker start` to start an existing container
- `docker rm` to delete a container

Use `docker ps`  to show running containers
- Add `-a` to show all containers

### Using nginx to create a homepage
For this task, the nginx container will be used to create a homepage.

To run the nginx container:
```bash
docker run -d -p 80:80 nginx
```

Accessing the container
```bash
docker exec -it 4f81caf0016b sh
```

To access the html directory
```bash
cd usr
cd share
cd nginx
cd html
# or alternatively
cd usr/share/nginx/html
```

To copy the file made in local machine to the container
```bash
docker cp "C:/Users/Ray/Git Repos/Eng89 Docker Introduction/index.html" 4f81caf0016b:usr/share/nginx/html/
```
## Automation using a Dockerfile
- Using a Dockerfile, an image can be built with the new `index.html`
- The Dockerfile needs to be in the same directory as the `index.html` or the path needs to be specified
- Create a Dockerfile using a text editor or using `nano Dockerfile`
### Writing the Dockerfile
- To state which existing image you want to build from:
```dockerfile
FROM nginx
```
- Adding a name label to the new image:
```dockerfile
LABEL MAINTAINER=Raymond_Sparta
```
- Copying the new `index.html` file to the image directory:
```dockerfile
COPY index.html /usr/share/nginx/html
```
- Specifying which port to run the container with:
```dockerfile
EXPOSE 80
```
- Configuring the container
```dockerfile
CMD ["nginx", "-g", "daemon off;"]
```
### Building the image
- To build the image we can use `docker_build`
- This uses the Dockerfile to build an image using the specifications provided
```bash
docker build -t docker_uname/image_name .
```
- Once the build is completed, the list of images used can be found by running:
```bash
docker images
```
### Running the container
Now that there is an custom image built, it can used to run a container.
```bash
docker run -d -p 30:80 docker_uname/image_name
```
The website will now be visible on `localhost:30` in a browser.
### Pushing to a Docker hub repository
The image can be pushed to a Docker hub repository so that it's backed up in the cloud and also other users can use the image.
To push it to a repo, use the following command:
```bash
docker push <hub-user>/<repo-name>:<tag>
```
- If the `repo-name` doesn't exist, it will create a new repository with that name
### Pulling from a Docker hub repository
To pull from a repository, the following command can be used:
```bash
docker run -d -p 50:80 raywlmo/eng89_nginx_automated .
```
This will pull the image from the repository and run a container using that custom image.

In this case, the website is running on `localhost:50`

## Docker Volumes

```bash
docker run -d -v C:/Users/Ray/Repos/Docker/:/usr/share/nginx/html -p 500:80 raywlmo/eng89_nginx_automated
```



prerequisites for an app to be containerised is 1) What are the depedencies
- Operating System
- Dependencies
- Ports