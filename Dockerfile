# build an image to automate the tasks to launch static website using nginx webserver with Docker

# Use the official image nginx
# label it with your name/email
# copy the folder/file from local host to the container
# declare to use default port 80 for nginx
# CMD with the final command - as an example `npm start`

# Building customised Docker images

# keyword to use an image - FROM

FROM nginx

# Label it with your email or name (Optional)

LABEL MAINTAINER=Raymond_Sparta

# Copy the data

COPY index.html /usr/share/nginx/html

# Adding the port

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]

# We get this from the official image of nginx
